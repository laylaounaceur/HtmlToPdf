#!/usr/bin/env node

var fs = require('fs')
var pdf = require('../')
var path = require('path')

var args = process.argv.slice(2)

if (args.length == 2 || args.length == 3) {
  htmlpdf(args[0], args[1],undefined,undefined,args[2])
} else if(args.length >=4) {
  htmlpdf(args[0], args[1],args[2],args[3],args[4])
}else {
	help()
}

function help () {
  var help = [
    'Usage: html-pdf <source> <destination>',
    'e.g.: html-pdf source.html destination.pdf'
  ].join('\n')

  console.log(help)
}

function htmlpdf (source, destination,width,height,orientation) {
  var html = fs.readFileSync(source, 'utf8')
  var options = {
    base: 'file://' + path.resolve(source),
   orientation: orientation || "portrait"
  }
  if(height && width){
		options['height']=height
		options['width']=width
} 
console.log("options",options);
  pdf.create(html, options).toFile(destination, function (err, res) {
    if (err) throw err
	console.log(path.resolve(destination))
  })
}
